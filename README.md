# PyAnDiff #

Compares the annotation of two PDF files.

## Examples

```
python script/andiff.py file1.pdf file2.pdf
```
