__author__ = 'Daniele Zambon'
__copyright__ = 'Copyright 2020, Daniele Zambon'
__license__ = 'BSD-3-Clause'
__version__ = '0.1.0'
__maintainer__ = 'Daniele Zambon'
__email__  = 'daniele.zambon@usi.ch'

from pyandiff.pyandiff import get_annotations, get_list_of_different_annotations, show_selected_annotations
from pyandiff.interfaces import andiff_cl, andiff_gui
