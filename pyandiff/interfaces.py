from pyandiff import get_annotations, get_list_of_different_annotations, show_selected_annotations

# https://www.geeksforgeeks.org/print-colors-python-terminal/
class fs: 
    reset='\033[0m'
    bold='\033[01m'
    disable='\033[02m'
    underline='\033[04m'
    reverse='\033[07m'
    strikethrough='\033[09m'
    invisible='\033[08m'
class fg: 
    black='\033[30m'
    red='\033[31m'
    green='\033[32m'
    orange='\033[33m'
    blue='\033[34m'
    purple='\033[35m'
    cyan='\033[36m'
    lightgrey='\033[37m'
    darkgrey='\033[90m' 
    lightred='\033[91m'
    lightgreen='\033[92m'
    yellow='\033[93m'
    lightblue='\033[94m'
    pink='\033[95m'
    lightcyan='\033[96m'
class bg: 
    black='\033[40m'
    red='\033[41m'
    green='\033[42m'
    orange='\033[43m'
    blue='\033[44m'
    purple='\033[45m'
    cyan='\033[46m'
    lightgrey='\033[47m'

def andiff_cl(args):
    """
    The current version of assume that the two PDF files differ only in the annotations.
    """

    # Compute the differences in annotations
    num_pages_old, annotations_old, inconsistent_old = get_annotations(pdf_fname=args.oldfile)
    num_pages_new, annotations_new, inconsistent_new = get_annotations(pdf_fname=args.newfile)
    ann_same, ann_removed, ann_added = get_list_of_different_annotations(annotations_old, annotations_new)
    any_difference = len(ann_added)>0 or len(ann_removed)>0

    # Recap the read files
    print()
    print(fs.bold, " ___ Files: _________________ ", fs.reset)
    print("LEFT  / OLD ) pages: {}, file: {}, inconsistencies: {}".format(num_pages_old, args.oldfile,
                                                                          inconsistent_old))
    print("RIGHT / NEW ) pages: {}, file: {}, inconsistencies: {}".format(num_pages_new, args.newfile,
                                                                          inconsistent_new))

    # Report to the console the differences in annotation
    print()
    print(fs.bold, " ___ Differences: _________________", fs.reset)
    if args.same:     
        for a in ann_same:   print(fg.blue,  "   ", a, fs.reset) 
    if args.removed:  
        for a in ann_removed:   print(fg.red,   "---", a, fs.reset)
    if args.added:      
        for a in ann_added:   print(fg.green, "+++", a, fs.reset)
    if not any_difference:
        print("No difference in annotation has been found.")

    return ann_same, ann_removed, ann_added

def andiff_gui(args, ann_same, ann_removed, ann_added):
    """
    The current version of assume that the two PDF files differ only in the annotations.
    """
    any_difference = len(ann_added)>0 or len(ann_removed)>0

    # Create a PDF with the differences highlighted
    if args.pdfout and any_difference:
        print()
        print(fs.bold, " ___ Output PDF: _________________", fs.reset)
        import tempfile
        temp = tempfile.NamedTemporaryFile() #2
        print("Creating one temporary file:", temp.name)
        try:
            show_selected_annotations(pdf_fname=args.newfile, pdf_out=temp.name, 
                                      annotations_added=ann_added, annotations_removed=ann_removed)
        finally:
            import subprocess
            cmd = ['xdg-open', temp.name] 
            out = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
            import time
            time.sleep(2)
            temp.close()

    # Call an external tool for comparing the text of the two PDF's
    if args.textdiff:
        cmd = ['diffpdf', args.oldfile, args.newfile] 
        import subprocess
        out = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

    return True