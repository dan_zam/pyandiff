class AnnotationNotCompliantError(Exception):
    # https://towardsdatascience.com/how-to-define-custom-exception-classes-in-python-bfa346629bca
    def __init__(self, *args):
        if args:
            self.message = args[0]
        else:
            self.message = None

    def __str__(self):
        print('calling str')
        if self.message:
            return 'AnnotationNotCompliantError, {} '.format(self.message)
        else:
            return 'AnnotationNotCompliantError has been raised'


class PDFAnnotation(object):
    """ Class storing a single annotation. """

    attr_to_check = ["/Type", "/Subtype", "/Rect"]
    attr_to_print = ["/Type", "/Subtype", "/Rect"]

    def __init__(self, pypdf2_obj, page=-1, filename=None):
        self.pypdf2_obj = pypdf2_obj  # PyPDF2 object
        self.page = page + 1# integer, page in the original pdf
        self.filename = filename
        
    def is_consistent(self, raise_error=False):
        for k in self.attr_to_check:
            if not k in self.pypdf2_obj.keys():
                rep = "No attribute {} in annotation on page {}, file {}:".format(k, self.page, self.filename)
                for key, val in self.pypdf2_obj.items():
                    rep += "\n\t{}: {}".format(key, val)
                for key in self.attr_to_check:
                    if not key in self.pypdf2_obj.keys():
                        rep += "\n\t{}: !missing!".format(key)
                if raise_error:
                    raise AnnotationNotCompliantError(rep)
                else:
                    print(rep)
                    return False
        return True

    def __eq__(self, other):
        assert isinstance(other, PDFAnnotation)
        for k in self.attr_to_check:
            if self.pypdf2_obj[k] != other.pypdf2_obj[k]:
                return False
        return True

    def __str__(self):
        res = "p{}) ".format(self.page if self.page >= 0 else '.n.d.')
        for k in self.attr_to_print:
            res += "{}: {}, ".format(k, self.pypdf2_obj[k])
        return res[:-2] 

    def get_rect(self):
        import parse
        import re  # https://www.dataquest.io/blog/regex-cheatsheet/
        rect = str(self.pypdf2_obj["/Rect"])
        rect = re.sub(r"\s*\[\s*", r"[", rect)
        rect = re.sub(r"\s*\]\s*", r"]", rect)
        rect = re.sub(r",", r" ", rect)
        rect = re.sub(r"\s+", r" ", rect)
        parser = parse.compile("[{} {} {} {}]")
        result = parser.parse(rect)
        assert len(result.spans) == 4
        return [float(result[i]) for i in range(4)]

def get_annotations(pdf_fname):
    """ 
    Extract the list of annotations present in the PDF. 
    See also https://stackoverflow.com/a/54823050
    """
    import PyPDF2

    pdffile = PyPDF2.PdfFileReader(open(pdf_fname, "rb"))
    num_pages = pdffile.getNumPages()

    inconsistent_annotations = 0
    ann_objs = []
    for i in range(num_pages) :
        page = pdffile.getPage(i)
        if "/Annots" in page.keys():
            for annot in page['/Annots']:
                ann_obj = PDFAnnotation(annot.getObject(), page=i, filename=pdf_fname)
                if ann_obj.is_consistent():
                    ann_objs.append(ann_obj)
                else:
                    inconsistent_annotations += 1
        # try:
        #     for annot in page['/Annots'] :
        #         ann_objs.append(PDFAnnotation(annot.getObject(), page=i))
        # except KeyError:
        #     pass
    return num_pages, ann_objs, inconsistent_annotations

def get_list_of_different_annotations(annotation_list_old, annotation_list_new):
    """
    Compare two lists of annotations and return the 
    - ann_same: a list of common annotations 
    - ann_add: a list of annotations present in list_new, but not in list_old
    - ann_removed: a list of annotations present in list_old, but not in list_new
    """
    ann_same = []
    ann_removed = []
    ann_added = []

    for ao in annotation_list_old:
        idx = -1
        for i in range(len(annotation_list_new)):
            if ao == annotation_list_new[i]:
                idx = i
                break
        if idx < 0:
            ann_removed.append(ao)
        else:
            ann_same.append(ao)
            annotation_list_new.pop(idx)

    for an in annotation_list_new:
        ann_added.append(an)

    return ann_same, ann_removed, ann_added

def show_selected_annotations(pdf_fname, pdf_out, annotations_added, annotations_removed):
    """ Create a PDF with highlighted the different annotations. """
    from pdf_annotate import PdfAnnotator, Location, Appearance
    pdf = PdfAnnotator(pdf_fname)
    def _add_this(ann, color):
        rect = ann.get_rect()
        pdf.add_annotation(
            'square',
            Location(x1=rect[0], y1=rect[1], x2=rect[2], y2=rect[3], page=ann.page),
            Appearance(stroke_color=color, stroke_width=3),
        )
    for ann in annotations_added:
        _add_this(ann, color=(0, 1, 0))
    for ann in annotations_removed:
        _add_this(ann, color=(1, 0, 0))
    pdf.write(pdf_out)
