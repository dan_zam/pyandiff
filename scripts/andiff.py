import argparse

from pyandiff import andiff_cl, andiff_gui

parser = argparse.ArgumentParser(description='Report the difference in annotations.')
parser.add_argument('oldfile', help="Older PDF file to be compared.")
parser.add_argument('newfile', help="More recent PDF file to be compared.")
# parser.add_argument('--diff', action='store_true', default=True, 
#                     help='Report the difference in annotations.')
parser.add_argument('--removed', action='store_true',
                    help='Report only the removed annotations.')
parser.add_argument('--added', action='store_true', 
                    help='Report only the added annotations.')
parser.add_argument('--same', action='store_true', 
                    help='Report the shared annotations.')
parser.add_argument('--pdfout', action='store_true', 
                    help='Assuming the two files differ only in annotation, '\
                         'it produces a file with highlighted in green the '\
                         'new annotations and in red the removed ones.')
parser.add_argument('--textdiff', action='store_true', 
                    help='Run the diffpdf utility for comparing the content '\
                         'of the the files.')
parser.add_argument('--all', action='store_true', 
                    help='All the above.')

if __name__ == "__main__":

    args = parser.parse_args()
    
    if not args.removed and not args.added:
        args.removed = True
        args.added = True

    if args.all:
        args.removed = True
        args.added = True
        args.same = True
        args.pdfout = True
        args.textdiff = True

    res_cl = andiff_cl(args)
    andiff_gui(args, *res_cl)    
