from distutils.core import setup

setup(
    name='PyAnDiff',
    version='0.1.0',
    packages=['pyandiff',],
    license='BSD-3-Clause',
    author='Daniele Zambon',
    author_email='daniele.zambon@usi.ch',
    #description=('...'),
    long_description=open('README.md').read(),
    install_requires=['pdf-annotate', 'PyPDF2', 'parse'],
    #url='https://github.com/...',
)



